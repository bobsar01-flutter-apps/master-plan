import 'package:flutter/material.dart';
import 'package:master_plan/controllers/plan_controller.dart';

/// InheritedWidget is used to pass data down to lower widgets in the tree
class PlanProvider extends InheritedWidget {
  final PlanController _controller = PlanController();

  PlanProvider({Key? key, required Widget child})
      : super(key: key, child: child);

  /// Flutter calls this method whenever the widget is rebuilt.
  ///
  /// In the updateShouldNotify method, you can look at the content of the old widget and determine if the child widgets need to be notified that the data has changed
  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;

  /// Enables the data to be accessible from anywhere in the app
  static PlanController of(BuildContext context) {
    // kick off the tree traversal process.
    // Flutter will start from the widget that owns this context and travel upward until it finds a PlanProvider.
    final PlanProvider? provider =
        context.dependOnInheritedWidgetOfExactType<PlanProvider>();
    return provider!._controller;
  }
}
