import 'repository.dart';

class InMemoryCache implements Repository {
  final Map<int, Model> _storage = <int, Model>{};
  int _lastKey = 0;

  @override
  Model create() {
    final int id = _lastKey + 1;
    _lastKey = id;

    final model = Model(id: id);
    _storage[id] = model;

    return model;
  }

  @override
  Model? get(int id) {
    return _storage[id];
  }

  /// Returns a fixed length list
  @override
  List<Model> getAll() {
    return _storage.values.toList(growable: false);
  }

  @override
  void update(Model item) {
    _storage[item.id] = item;
  }

  @override
  void clear() {
    _storage.clear();
  }

  @override
  void delete(Model item) {
    _storage.remove(item.id);
  }
}
