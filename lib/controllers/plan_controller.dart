import 'package:master_plan/models/plan.dart';
import 'package:master_plan/models/task.dart';
import 'package:master_plan/services/plan_services.dart';

class PlanController {
  final PlanServices services = PlanServices();
  // This public getter cannot be modified by any other object
  List<Plan> get plans => List.unmodifiable(services.getAllPlans());

  /// add a new plan
  void addNewPlan(String name) {
    if (name.isEmpty) return;

    final planNames = plans.map((plan) => plan.name);
    name = _checkForDuplicates(planNames, name);

    services.createPlan(name);
  }

  void savePlan(Plan plan) {
    services.savePlan(plan);
  }

  /// delete a plan
  void deletePlan(Plan plan) {
    services.delete(plan);
  }

  void createNewTask(Plan plan, [String? description]) {
    if (description == null || description.isEmpty) {
      description = 'New Task';
    }
    description = _checkForDuplicates(
        plan.tasks.map((task) => task.description), description);

    services.addTask(plan, description);
  }

  void deleteTask(Plan plan, Task task) {
    services.deleteTask(plan, task);
  }

  String _checkForDuplicates(Iterable<String> items, String text) {
    final int duplicatedCount =
        items.where((item) => item.contains(text)).length;
    if (duplicatedCount > 0) {
      text += '${duplicatedCount + 1}';
    }
    return text;
  }
}
